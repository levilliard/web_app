
package com.levilliard.atouri.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import java.util.ArrayList;
import com.levilliard.atouri.beans.Message;
import com.levilliard.atouri.util.DBConnection;
import java.sql.Timestamp;


public class MessageDAO  extends DAO<Message>{
    private final Connection conn;

    public MessageDAO(){
        conn = DBConnection.getConnection();
    }

    @Override
    public Message find(String id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Message> findByCriteria(String type){
        throw new UnsupportedOperationException("Not supported yet.");
    }
    
    @Override
    public List<Message> findAll() {    
        List<Message> m_list = new ArrayList<>();
        PreparedStatement pstm = null;
        ResultSet rs = null;
       
        /* prepare the query */
        String query = "SELECT * FROM ATOURI.MESSAGE ";
        
        try{
            pstm = conn.prepareStatement(query);
            rs = pstm.executeQuery();
            
            while(rs.next()){
                Message msg = new Message();
                msg.setMessageId(rs.getInt("MESSAGE_ID"));
                msg.sethUserId(rs.getInt("HUSER_ID"));
                msg.setTouristAreaId(rs.getInt("TOURIST_AREA_ID"));
                msg.setMessageContent(rs.getString("MESSAGE_CONTENT"));
                msg.setMessageDate(rs.getString("MESSAGE_DATETIME"));

                m_list.add(msg);             
            } 
        }catch(SQLException ex){
            ex.printStackTrace();
        }finally{
            /* close rs and pstm */
           if(rs != null){
               try{
                   rs.close();
               }catch(SQLException ex){

               }
           }

           if(pstm != null){
               try{
                   pstm.close();
               }catch(SQLException ex){

               }
           }
        }
       
       return m_list;
    }

   
    public List<Message> findSomeData(String area_id) {    
        List<Message> m_list = new ArrayList<>();
        PreparedStatement pstm = null;
        ResultSet rs = null;
       
        /* prepare the query */
        String query = "SELECT * FROM ATOURI.MESSAGE WHERE TOURIST_AREA_ID = ? ";
        
        try{
            pstm = conn.prepareStatement(query);
            pstm.setInt(1, Integer.parseInt(area_id));
            rs = pstm.executeQuery();
            
            while(rs.next()){
                Message msg = new Message();
                msg.setMessageId(rs.getInt("MESSAGE_ID"));
                msg.sethUserId(rs.getInt("HUSER_ID"));
                msg.setTouristAreaId(rs.getInt("TOURIST_AREA_ID"));
                msg.setMessageContent(rs.getString("MESSAGE_CONTENT"));
                msg.setMessageDate(rs.getString("MESSAGE_DATETIME"));

                m_list.add(msg);             
            } 
        }catch(SQLException ex){
            ex.printStackTrace();
        }finally{
            /* close rs and pstm */
           if(rs != null){
               try{
                   rs.close();
               }catch(SQLException ex){

               }
           }

           if(pstm != null){
               try{
                   pstm.close();
               }catch(SQLException ex){

               }
           }
        }
       
       return m_list;
    }

    
    @Override
    public boolean create(Message obj) {
        boolean status = false;
        StringBuilder sb = new StringBuilder("INSERT INTO ATOURI.MESSAGE(");
        sb.append("HUSER_ID, ");
        sb.append("TOURIST_AREA_ID, ");
        sb.append("MESSAGE_CONTENT, ");
        sb.append("MESSAGE_DATETIME) ");        
        sb.append("VALUES(?, ?, ?, ?)");
        
        PreparedStatement stm = null;
        
        try{
            if(conn == null){
                status = false;
            }
            else{
                Timestamp ts = Timestamp.valueOf(obj.getMessageDate().trim());
                stm = conn.prepareStatement(sb.toString());
                stm.setInt(1, obj.gethUserId());
                stm.setInt(2, obj.getTouristAreaId());
                stm.setObject(3, obj.getMessageContent());
                stm.setObject(4, ts);
  
                stm.executeUpdate();
                status = true;
            }
        }catch(SQLException ex){
            status = false;
            ex.printStackTrace();
        }finally{        
            if(stm != null){
                try{
                    stm.close();
                }catch(SQLException ex){

                }
            }
        }
        
        return status;
    }

    @Override
    public boolean update(Message obj) {
        boolean status = false;
        StringBuilder sb = new StringBuilder("UPDATE ATOURI.MESSAGE ");
        sb.append("SET HUSER_ID = ?, ");
        sb.append("TOURIST_AREA_ID = ?, ");
        sb.append("MESSAGE_CONTENT = ?, ");
        sb.append("MESSAGE_DATETIME = ? ");
        sb.append("WHERE MESSAGE_ID = ?");
        
        PreparedStatement stm = null;

        try{
            Timestamp ts = Timestamp.valueOf(obj.getMessageDate().trim());
            stm = conn.prepareStatement(sb.toString());
            stm.setInt(1, obj.gethUserId());
            stm.setInt(2, obj.getTouristAreaId());
            stm.setObject(3, obj.getMessageContent());
            stm.setObject(4, ts);
            stm.setInt(5, obj.getMessageId());
            stm.executeUpdate();
            
            status = true;

        }catch(SQLException ex){
            ex.printStackTrace();
        }finally{
            if(stm != null){
                try{
                    stm.close();
                }catch(SQLException ex){
                    
                }
            }
        }
        
        return status;
    }
    @Override
    public boolean delete(String id) {
        String query = " DELETE FROM ATOURI.MESSAGE WHERE MESSAGE_ID = ? ";
        PreparedStatement stm = null;
        boolean status = false;
        
        try{
            stm = conn.prepareStatement(query);
            stm.setInt(1, Integer.parseInt(id));
            stm.executeUpdate();   
            status = true;         
        }catch(SQLException ex){
            
        }finally{
            if(stm != null){
                try{
                    stm.close();
                }catch(SQLException ex){
                    
                }
            }
        }
        
        return status;
    }
}
