
package com.levilliard.atouri.dao;

/**
 *
 * @author levilliard
 */
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import java.util.ArrayList;
import java.time.LocalDate;

import com.levilliard.atouri.beans.TouristArea;
import com.levilliard.atouri.util.DBConnection;
import java.sql.Timestamp;


public class TouristAreaDAO extends DAO<TouristArea>{
    private final Connection conn;

    public TouristAreaDAO(){
        conn = DBConnection.getConnection();
    }

    @Override
    public TouristArea find(String id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    
    @Override
    public List<TouristArea>findAll(){
        throw new UnsupportedOperationException("Not supported yet.");
    }
    
    
    @Override
    public List<TouristArea> findByCriteria(String type) {    
        List<TouristArea> m_list = new ArrayList<>();
        
        if(type == null || type.length() == 0){
            return m_list;
        }
        
        PreparedStatement pstm = null;
        ResultSet rs = null;
       
        /* prepare the query */
        String query = "SELECT * FROM ATOURI.TOURIST_AREA WHERE TOURIST_AREA_MODIFY_BY = ? AND tourist_area_cat = ? ";
        final String adm = "tizondife";
        
        try{
            pstm = conn.prepareStatement(query);
            pstm.setString(1, adm);
            pstm.setString(2, type);

            System.out.println(pstm.toString());
            rs = pstm.executeQuery();
            
            while(rs.next()){
                TouristArea ta = new TouristArea();
                ta.setTouristAreaId(rs.getInt("TOURIST_AREA_ID") + "");
                ta.setCommuneId(rs.getString("COMMUNE_ID"));
                ta.setSectionCommId(rs.getString("SECTION_COMM_ID"));
                ta.setTouristAreaName(rs.getString("TOURIST_AREA_NAME"));
                ta.setTouristAreaDesc(rs.getString("TOURIST_AREA_DESC"));
                ta.setTouristAreaImg(rs.getString("TOURIST_AREA_IMG"));
                ta.setTouristAreaDetails(rs.getString("TOURIST_AREA_DETAILS"));
                ta.setTouristAreaLoc(rs.getString("TOURIST_AREA_LOC"));
                ta.setTouristAreaLat(rs.getString("TOURIST_AREA_LAT"));
                ta.setTouristAreaLong(rs.getString("TOURIST_AREA_LONG"));
                ta.setTouristAreaCreatedBy(rs.getString("TOURIST_AREA_CREATED_BY"));
                ta.setTouristAreaDateCreated(rs.getString("TOURIST_AREA_DATE_CREATED"));
                ta.setTouristAreaModifyBy(rs.getString("TOURIST_AREA_MODIFY_BY"));
                ta.setTouristAreaDateModify(rs.getString("TOURIST_AREA_DATE_MODIFY"));
                ta.setTouristAreaType(rs.getString("TOURIST_AREA_TYPE"));
                ta.setTouristAreaCat(rs.getString("TOURIST_AREA_CAT"));
                m_list.add(ta);
            } 
        }catch(SQLException ex){
            ex.printStackTrace();
        }finally{
            /* close rs and pstm */
           if(rs != null){
               try{
                   rs.close();
               }catch(SQLException ex){

               }
           }

           if(pstm != null){
               try{
                   pstm.close();
               }catch(SQLException ex){

               }
           }
        }
       
       return m_list;
    }

    @Override
    public boolean create(TouristArea obj) {
        
        boolean status = false;
        
        final String query = "INSERT INTO ATOURI.TOURIST_AREA(COMMUNE_ID, SECTION_COMM_ID, TOURIST_AREA_NAME, TOURIST_AREA_DESC, TOURIST_AREA_IMG, TOURIST_AREA_DETAILS, TOURIST_AREA_LOC, TOURIST_AREA_LAT, TOURIST_AREA_LONG, TOURIST_AREA_CREATED_BY, TOURIST_AREA_DATE_CREATED, TOURIST_AREA_MODIFY_BY, TOURIST_AREA_DATE_MODIFY, TOURIST_AREA_TYPE, TOURIST_AREA_CAT) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ? ,?, ?, ?, ?)";
        PreparedStatement stm = null;
        
        try{
            if(conn == null){
                System.out.println("Null value found");
                status = false;
            }
            else{
                Timestamp ts1;
                ts1 = Timestamp.valueOf(obj.getTouristAreaDateCreated());                 
                
                Timestamp ts2;
                ts2 = Timestamp.valueOf(obj.getTouristAreaDateModify());                
                
                stm = conn.prepareStatement(query);
                stm.setInt(1, Integer.parseInt(obj.getCommuneId()));
                stm.setInt(2, Integer.parseInt(obj.getSectionCommId()));
                stm.setString(3, obj.getTouristAreaName());
                stm.setString(4, obj.getTouristAreaDesc());
                stm.setString(5, obj.getTouristAreaImg());
                stm.setString(6, obj.getTouristAreaDetails());
                stm.setString(7, obj.getTouristAreaLoc());
                stm.setDouble(8, Double.parseDouble(obj.getTouristAreaLat()));
                stm.setDouble(9, Double.parseDouble(obj.getTouristAreaLong()));
                stm.setString(10, obj.getTouristAreaCreatedBy());
                stm.setObject(11, ts1);
                stm.setString(12, obj.getTouristAreaModifyBy());
                stm.setObject(13, ts2);
                stm.setString(14, obj.getTouristAreaType());
                stm.setString(15, obj.getTouristAreaCat());
                stm.executeUpdate();
                status = true;
            }
        }catch(SQLException ex){
            status = false;
            ex.printStackTrace();
        }finally{        
            if(stm != null){
                try{
                    stm.close();
                }catch(SQLException ex){

                }
            }
        }
        
        return status;
    }

    @Override
    public boolean update(TouristArea obj) {
        boolean status = false;
        StringBuilder query_builder = new StringBuilder();
        query_builder.append("UPDATE ATOURI.TOURIST_AREA " );
        query_builder.append("SET COMMUNE_ID = ?, " );
        query_builder.append("SECTION_COMM_ID = ?, " );
        query_builder.append("TOURIST_AREA_NAME = ?, " );
        query_builder.append("TOURIST_AREA_DESC = ?, " );
        query_builder.append("TOURIST_AREA_IMG = ?, " );
        query_builder.append("TOURIST_AREA_DETAILS = ?, " );
        query_builder.append("TOURIST_AREA_LOC = ?, " );
        query_builder.append("TOURIST_AREA_LAT = ?, " );
        query_builder.append("TOURIST_AREA_LONG = ?, " );
        query_builder.append("TOURIST_AREA_CREATED_BY = ?, " );
        query_builder.append("TOURIST_AREA_DATE_CREATED = ?, " );
        query_builder.append("TOURIST_AREA_MODIFY_BY = ?, " );
        query_builder.append("TOURIST_AREA_DATE_MODIFY = ?, " );
        query_builder.append("TOURIST_AREA_TYPE = ?, ");
        query_builder.append("TOURIST_AREA_CAT = ? ");
        query_builder.append("WHERE TOURIST_AREA_ID = ? " );

        PreparedStatement stm = null;

        try{
            Timestamp ts1;
            ts1 = Timestamp.valueOf(obj.getTouristAreaDateCreated());                 

            Timestamp ts2;
            ts2 = Timestamp.valueOf(obj.getTouristAreaDateModify());                

            stm = conn.prepareStatement(query_builder.toString());
            stm.setInt(1, Integer.parseInt(obj.getCommuneId()));
            stm.setInt(2, Integer.parseInt(obj.getSectionCommId()));
            stm.setString(3, obj.getTouristAreaName());
            stm.setString(4, obj.getTouristAreaDesc());
            stm.setString(5, obj.getTouristAreaImg());
            stm.setString(6, obj.getTouristAreaDetails());
            stm.setString(7, obj.getTouristAreaLoc());
            stm.setDouble(8, Double.parseDouble(obj.getTouristAreaLat()));
            stm.setDouble(9, Double.parseDouble(obj.getTouristAreaLong()));
            stm.setString(10, obj.getTouristAreaCreatedBy());
            stm.setObject(11, ts1);
            stm.setString(12, obj.getTouristAreaModifyBy());
            stm.setObject(13, ts2);
            stm.setString(14, obj.getTouristAreaType());
            stm.setString(15, obj.getTouristAreaCat());
            stm.setInt(16, Integer.parseInt(obj.getTouristAreaId()));
            stm.executeUpdate();
            status = true;
        }catch(SQLException ex){
            ex.printStackTrace();
        }finally{
            if(stm != null){
                try{
                    stm.close();
                }catch(SQLException ex){
                    
                }
            }
        }
        
        return status;
    }
    @Override
    public boolean delete(String id) {
        String query = " DELETE FROM ATOURI.TOURIST_AREA WHERE TOURIST_AREA_ID = ? ";
        PreparedStatement stm = null;
        boolean status = false;
        
        try{
            stm = conn.prepareStatement(query);
            stm.setInt(1, Integer.parseInt(id));
            stm.executeUpdate();     
            status = true;       
        }catch(SQLException ex){
            ex.printStackTrace();
        }finally{
            if(stm != null){
                try{
                    stm.close();
                }catch(SQLException ex){
                    
                }
            }
        }
        
        return status;
    }
}
