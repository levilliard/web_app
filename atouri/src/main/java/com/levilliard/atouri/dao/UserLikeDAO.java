
package com.levilliard.atouri.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import com.levilliard.atouri.beans.UserLike;
import com.levilliard.atouri.util.DBConnection;
import java.sql.Timestamp;


public class UserLikeDAO  extends DAO<UserLike>{
    private final Connection conn;

    public UserLikeDAO(){
        conn = DBConnection.getConnection();
    }

    @Override
    public UserLike find(String id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    
    @Override
    public List<UserLike> findByCriteria(String id){
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    public int findLike(String like, int tId){
        int count_like = 0;
        PreparedStatement pstm = null;
        ResultSet rs = null;
        String query = "";
       
        if(like == null || like.length() == 0){
            return count_like;
        }
        /* prepare the query */
        if("like".equalsIgnoreCase(like)){
            query = "SELECT COUNT(USER_LIKE_VALUE) COUNT_LIKE FROM ATOURI.USER_LIKE WHERE TOURIST_AREA_ID = ? AND USER_LIKE_VALUE = 1";
        }else if("unlike".equalsIgnoreCase(like)){
            query = "SELECT COUNT(USER_UNLIKE_VALUE) COUNT_LIKE FROM ATOURI.USER_LIKE WHERE TOURIST_AREA_ID = ? AND USER_LIKE_VALUE = 0";
        }
        
        try{
            pstm = conn.prepareStatement(query);
            pstm.setInt(1, tId);
            rs = pstm.executeQuery();

            if(rs.next()){
              count_like = rs.getInt("COUNT_LIKE");
            } 
        }catch(SQLException ex){
            ex.printStackTrace();
        }finally{
            /* close rs and pstm */
           if(rs != null){
               try{
                   rs.close();
               }catch(SQLException ex){

               }
           }

           if(pstm != null){
               try{
                   pstm.close();
               }catch(SQLException ex){

               }
           }
        }
       
       return count_like;
    }

    @Override
    public List<UserLike> findAll() {    
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    @Override
    public boolean create(UserLike obj) {
            
        boolean status = false;
        StringBuilder sb = new StringBuilder("INSERT INTO ATOURI.USER_LIKE(");
        sb.append("HUSER_ID, ");
        sb.append("TOURIST_AREA_ID, ");
        sb.append("USER_LIKE_VALUE, ");
        sb.append("USER_UNLIKE_VALUE, ");
        sb.append("USER_LIKE_DATE_CREATED) ");        
        sb.append("VALUES(?, ?, ?, ?, ?)");
        
        PreparedStatement stm = null;
        
        try{
            if(conn == null){
                status = false;
            }
            else{
                Timestamp ts = Timestamp.valueOf(obj.getUserLikeDateCreated().trim());
                stm = conn.prepareStatement(sb.toString());
                stm.setInt(1,obj.gethUserId());
                stm.setInt(2,obj.getTouristAreaId());
                stm.setInt(3, obj.getUserLikeValue());
                stm.setInt(4, obj.getUserUnlike());
                stm.setObject(5, ts);
  
                stm.executeUpdate();
                status = true;
            }
        }catch(SQLException ex){
            status = false;
            ex.printStackTrace();
        }finally{        
            if(stm != null){
                try{
                    stm.close();
                }catch(SQLException ex){

                }
            }
        }
        
        return status;
    }

    @Override
    public boolean update(UserLike obj) {
        boolean status = false;
        StringBuilder sb = new StringBuilder("UPDATE ATOURI.USER_LIKE ");
        sb.append("SET HUSER_ID = ?, ");
        sb.append("TOURIST_AREA_ID = ?, ");
        sb.append("USER_LIKE_VALUE = ?, ");
        sb.append("USER_UNLIKE_VALUE = ?, ");
        sb.append("USER_LIKE_DATE_CREATED = ? ");  
        sb.append("WHERE USER_LIKE_ID = ?");
        
        PreparedStatement stm = null;

        try{
            Timestamp ts = Timestamp.valueOf(obj.getUserLikeDateCreated().trim());
            stm = conn.prepareStatement(sb.toString());
            stm.setInt(1,obj.gethUserId());
            stm.setInt(2,obj.getTouristAreaId());
            stm.setInt(3, obj.getUserLikeValue());
            stm.setInt(4, obj.getUserUnlike());
            stm.setObject(5, ts);
            stm.setInt(6, obj.getUserLikeId());
            stm.executeUpdate();
            
            status = true;

        }catch(SQLException ex){
            ex.printStackTrace();
        }finally{
            if(stm != null){
                try{
                    stm.close();
                }catch(SQLException ex){
                    
                }
            }
        }
        
        return status;
    }
    @Override
    public boolean delete(String id) {
        String query = " DELETE FROM ATOURI.USER_LIKE WHERE USER_LIKE_ID = ? ";
        PreparedStatement stm = null;
        boolean status = false;
        
        try{
            stm = conn.prepareStatement(query);
            stm.setInt(1, Integer.parseInt(id));
            stm.executeUpdate();   
            status = true;         
        }catch(SQLException ex){
            
        }finally{
            if(stm != null){
                try{
                    stm.close();
                }catch(SQLException ex){
                    
                }
            }
        }
        
        return status;
    }
}
