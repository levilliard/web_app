
package com.levilliard.atouri.dao;

/**
 *
 * @author levilliard
 */

import java.sql.Connection;
import com.levilliard.atouri.util.DBConnection;
import java.util.List;

public abstract class DAO<T> {

	public Connection connect = DBConnection.getConnection();
	
	/**
	 * Permet de récupérer un objet via son ID
	 * @param id
	 * @return
	 */
	public abstract T find(String id);
        
        public abstract List<T> findAll();
        
        public abstract List<T> findByCriteria(String type);
        
	/**
	 * Permet de créer une entrée dans la base de données
	 * par rapport à un objet
	 * @param obj
	 */
	public abstract boolean create(T obj);
	
	/**
	 * Permet de mettre à jour les données d'une entrée dans la base 
	 * @param obj
	 */
	public abstract boolean update(T obj);
	
	/**
	 * Permet la suppression d'une entrée de la base
	 * @param obj
	 */
	public abstract boolean delete(String id);
}