
package com.levilliard.atouri.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import java.util.ArrayList;
import java.time.LocalDate;
import com.levilliard.atouri.beans.User;
import com.levilliard.atouri.util.DBConnection;


public class UserDAO  extends DAO<User>{
    private final Connection conn;

    public UserDAO(){
        conn = DBConnection.getConnection();
    }

    @Override
    public User find(String id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
   
    public User userLogin(User fakeUser){

        PreparedStatement pstm = null;
        ResultSet rs = null;
        User usr = null;
        final String query = "SELECT * FROM ATOURI.HUSER WHERE HUSER_USERNAME = ? AND HUSER_PASSWORD = ? ";
        System.out.println("Login u and p: " + fakeUser.gethUserUsername().trim() + ":" + fakeUser.gethUserPassword().trim());

        try{
            pstm = conn.prepareStatement(query);
            pstm.setString(1, fakeUser.gethUserUsername().trim());
            pstm.setString(2, fakeUser.gethUserPassword().trim());
            rs = pstm.executeQuery();   
            
            if(rs.next()){
                usr = new User();
                usr.sethUserId(rs.getString("HUSER_ID"));
                usr.sethUserFirstname(rs.getString("HUSER_FIRSTNAME"));
                usr.sethUserLastname(rs.getString("HUSER_LASTNAME"));
                usr.sethUserTitle(rs.getString("HUSER_TITLE"));
                usr.sethUserSex(rs.getString("HUSER_SEX"));
                usr.sethUserPhone(rs.getString("HUSER_PHONE"));
                usr.sethUserEmail(rs.getString("HUSER_EMAIL"));
                usr.sethUserUsername(rs.getString("HUSER_USERNAME"));
                usr.sethUserPassword(rs.getString("HUSER_PASSWORD"));
                usr.sethUserCode(rs.getString("HUSER_CODE"));
                usr.sethUserImg(rs.getString("HUSER_IMG"));
                usr.sethUserDetails(rs.getString("HUSER_DETAILS"));
                usr.sethUserDate(rs.getString("HUSER_DATE"));
            }            
        }catch(SQLException ex){
            
        }finally{
            if(null != rs){
               try{
                   rs.close();
               }catch(SQLException ex){

               }
           }
            if(pstm != null){
                try{
                    pstm.close();
                }catch(SQLException ex){
                    
                }
            }
        }
        
        return usr;
    }

    
    @Override
    public List<User> findByCriteria(String type){
        throw new UnsupportedOperationException("Not supported yet.");
    }
    
    @Override
    public List<User> findAll() {    
        List<User> m_list = new ArrayList<>();
        PreparedStatement pstm = null;
        ResultSet rs = null;
       
        /* prepare the query */
        String query = "SELECT * FROM ATOURI.HUSER ";
        
        try{
            pstm = conn.prepareStatement(query);
            rs = pstm.executeQuery();

            while(rs.next()){
                User usr = new User();
                usr.sethUserId(rs.getString("HUSER_ID"));
                usr.sethUserFirstname(rs.getString("HUSER_FIRSTNAME"));
                usr.sethUserLastname(rs.getString("HUSER_LASTNAME"));
                usr.sethUserTitle(rs.getString("HUSER_TITLE"));
                usr.sethUserSex(rs.getString("HUSER_SEX"));
                usr.sethUserPhone(rs.getString("HUSER_PHONE"));
                usr.sethUserEmail(rs.getString("HUSER_EMAIL"));
                usr.sethUserUsername(rs.getString("HUSER_USERNAME"));
                usr.sethUserPassword(rs.getString("HUSER_PASSWORD"));
                usr.sethUserCode(rs.getString("HUSER_CODE"));
                usr.sethUserImg(rs.getString("HUSER_IMG"));
                usr.sethUserDetails(rs.getString("HUSER_DETAILS"));
                usr.sethUserDate(rs.getString("HUSER_DATE"));
                m_list.add(usr);             
            } 
        }catch(SQLException ex){
            ex.printStackTrace();
        }finally{
            /* close rs and pstm */
           if(rs != null){
               try{
                   rs.close();
               }catch(SQLException ex){

               }
           }

           if(pstm != null){
               try{
                   pstm.close();
               }catch(SQLException ex){

               }
           }
        }
       
       return m_list;
    }

    @Override
    public boolean create(User obj) {

        boolean status = false;
        StringBuilder sb = new StringBuilder("INSERT INTO ATOURI.HUSER(");
        sb.append("HUSER_FIRSTNAME, ");
        sb.append("HUSER_LASTNAME, ");
        sb.append("HUSER_TITLE, ");
        sb.append("HUSER_SEX, ");
        sb.append("HUSER_PHONE, ");
        sb.append("HUSER_EMAIL, ");    
        sb.append("HUSER_USERNAME, ");
        sb.append("HUSER_PASSWORD, ");
        sb.append("HUSER_CODE, ");
        sb.append("HUSER_IMG, ");   
        sb.append("HUSER_DETAILS, ");
        sb.append("HUSER_DATE) ");
        sb.append("VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
        
        PreparedStatement stm = null;
        
        try{
            if(conn == null){
                status = false;
            }
            else{
                stm = conn.prepareStatement(sb.toString());
                LocalDate d1 = LocalDate.parse(obj.gethUserDate().trim());
                stm = conn.prepareStatement(sb.toString());
                stm.setString(1, obj.gethUserFirstname());
                stm.setString(2, obj.gethUserLastname());
                stm.setString(3, obj.gethUserTitle());
                stm.setString(4, obj.gethUserSex());
                stm.setString(5, obj.gethUserPhone());
                stm.setString(6, obj.gethUserEmail());
                stm.setString(7, obj.gethUserUsername());
                stm.setString(8, obj.gethUserPassword());
                stm.setString(9, obj.gethUserCode());
                stm.setString(10, obj.gethUserImg());
                stm.setString(11, obj.gethUserDetails());
                stm.setObject(12, d1);
  
                stm.executeUpdate();
                status = true;
            }
        }catch(SQLException ex){
            status = false;
            ex.printStackTrace();
        }finally{        
            if(stm != null){
                try{
                    stm.close();
                }catch(SQLException ex){

                }
            }
        }
        
        return status;
    }

    @Override
    public boolean update(User obj) {
        boolean status = false;
        StringBuilder sb = new StringBuilder("UPDATE ATOURI.HUSER ");
        sb.append("SET HUSER_FIRSTNAME = ?, ");
        sb.append("HUSER_LASTNAME = ?, ");
        sb.append("HUSER_TITLE = ?, ");
        sb.append("HUSER_SEX = ?, ");
        sb.append("HUSER_PHONE = ?, ");
        sb.append("HUSER_EMAIL = ?, ");    
        sb.append("HUSER_USERNAME = ?, ");
        sb.append("HUSER_PASSWORD = ?, ");
        sb.append("HUSER_CODE = ?, ");
        sb.append("HUSER_IMG = ?, ");   
        sb.append("HUSER_DETAILS = ?, ");
        sb.append("HUSER_DATE = ? ");
        sb.append("WHERE HUSER_ID = ?");
        
        PreparedStatement stm = null;

        try{
            stm = conn.prepareStatement(sb.toString());
            LocalDate d1 = LocalDate.parse(obj.gethUserDate().trim());
            stm.setString(1, obj.gethUserFirstname());
            stm.setString(2, obj.gethUserLastname());
            stm.setString(3, obj.gethUserTitle());
            stm.setString(4, obj.gethUserSex());
            stm.setString(5, obj.gethUserPhone());
            stm.setString(6, obj.gethUserEmail());
            stm.setString(7, obj.gethUserUsername());
            stm.setString(8, obj.gethUserPassword());
            stm.setString(9, obj.gethUserCode());
            stm.setString(10, obj.gethUserImg());
            stm.setString(11, obj.gethUserDetails());
            stm.setObject(12, d1);
            stm.setInt(11, Integer.parseInt(obj.gethUserId()));
            stm.executeUpdate();
            
            status = true;

        }catch(SQLException ex){
            ex.printStackTrace();
        }finally{
            if(stm != null){
                try{
                    stm.close();
                }catch(SQLException ex){
                    
                }
            }
        }
        
        return status;
    }
    @Override
    public boolean delete(String id) {
        String query = " DELETE FROM ATOURI.HUSER WHERE HUSER_ID = ? ";
        PreparedStatement stm = null;
        boolean status = false;
        
        try{
            stm = conn.prepareStatement(query);
            stm.setInt(1, Integer.parseInt(id));
            stm.executeUpdate();   
            status = true;         
        }catch(SQLException ex){
            
        }finally{
            if(stm != null){
                try{
                    stm.close();
                }catch(SQLException ex){
                    
                }
            }
        }
        
        return status;
    }
}
