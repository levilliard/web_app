
package com.levilliard.atouri.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import java.util.ArrayList;
import com.levilliard.atouri.beans.UserMessage;
import com.levilliard.atouri.util.DBConnection;
import java.sql.Timestamp;


public class UserMessageDAO  extends DAO<UserMessage>{
    private final Connection conn;

    public UserMessageDAO(){
        conn = DBConnection.getConnection();
    }

    @Override
    public UserMessage find(String id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<UserMessage> findByCriteria(String type){
        throw new UnsupportedOperationException("Not supported yet.");
    }
    
    @Override
    public List<UserMessage> findAll() {    
        List<UserMessage> m_list = new ArrayList<>();
        PreparedStatement pstm = null;
        ResultSet rs = null;
       
        /* prepare the query */
        String query = "SELECT * FROM ATOURI.USER_MESSAGE ORDER BY USER_MESSAGE_DATETIME ASC LIMIT 100 ";
        
        try{
            pstm = conn.prepareStatement(query);
            rs = pstm.executeQuery();
            
            while(rs.next()){
                UserMessage msg = new UserMessage();
                msg.setUserMessageId(rs.getInt("USER_MESSAGE_ID"));
                msg.sethUserId(rs.getInt("HUSER_ID"));
                msg.sethUserUsername(rs.getString("HUSER_USERNAME"));
                msg.setUserMessageContent(rs.getString("USER_MESSAGE_CONTENT"));
                msg.setUserMessageDate(rs.getString("USER_MESSAGE_DATETIME"));

                m_list.add(msg);             
            } 
        }catch(SQLException ex){
            ex.printStackTrace();
        }finally{
            /* close rs and pstm */
           if(rs != null){
               try{
                   rs.close();
               }catch(SQLException ex){

               }
           }

           if(pstm != null){
               try{
                   pstm.close();
               }catch(SQLException ex){

               }
           }
        }
       
       return m_list;
    }

    
    @Override
    public boolean create(UserMessage obj) {
        boolean status = false;
        StringBuilder sb = new StringBuilder("INSERT INTO ATOURI.USER_MESSAGE(");
        sb.append("HUSER_ID, ");
        sb.append("HUSER_USERNAME, ");
        sb.append("USER_MESSAGE_CONTENT, ");
        sb.append("USER_MESSAGE_DATETIME) ");        
        sb.append("VALUES(?, ?, ?, ?)");
        
        PreparedStatement stm = null;
        
        try{
            if(conn == null){
                status = false;
            }
            else{
                Timestamp ts = Timestamp.valueOf(obj.getUserMessageDate().trim());
                stm = conn.prepareStatement(sb.toString());
                stm.setInt(1, obj.gethUserId());
                stm.setString(2, obj.gethUserUsername());
                stm.setObject(3, obj.getUserMessageContent());
                stm.setObject(4, ts);
  
                stm.executeUpdate();
                status = true;
            }
        }catch(SQLException ex){
            status = false;
            ex.printStackTrace();
        }finally{        
            if(stm != null){
                try{
                    stm.close();
                }catch(SQLException ex){

                }
            }
        }
        
        return status;
    }

 
    @Override
    public boolean update(UserMessage obj) {
        boolean status = false;
        StringBuilder sb = new StringBuilder("UPDATE ATOURI.USER_MESSAGE ");
        sb.append("SET HUSER_ID = ?, ");
        sb.append("HUSER_USERNAME = ?, ");
        sb.append("USER_MESSAGE_CONTENT = ?, ");
        sb.append("USER_MESSAGE_DATETIME = ? ");
        sb.append("WHERE USER_MESSAGE_ID = ?");
        
        PreparedStatement stm = null;

        try{
            Timestamp ts = Timestamp.valueOf(obj.getUserMessageDate().trim());
            stm = conn.prepareStatement(sb.toString());
            stm.setInt(1, obj.gethUserId());
            stm.setString(2, obj.gethUserUsername());
            stm.setObject(3, obj.getUserMessageContent());
            stm.setObject(4, ts);
            stm.setInt(5, obj.getUserMessageId());
            stm.executeUpdate();
            
            status = true;

        }catch(SQLException ex){
            ex.printStackTrace();
        }finally{
            if(stm != null){
                try{
                    stm.close();
                }catch(SQLException ex){
                    
                }
            }
        }
        
        return status;
    }
    
    @Override
    public boolean delete(String id) {
        String query = " DELETE FROM ATOURI.USER_MESSAGE WHERE USER_MESSAGE_ID = ? ";
        PreparedStatement stm = null;
        boolean status = false;
        
        try{
            stm = conn.prepareStatement(query);
            stm.setInt(1, Integer.parseInt(id));
            stm.executeUpdate();   
            status = true;         
        }catch(SQLException ex){
            
        }finally{
            if(stm != null){
                try{
                    stm.close();
                }catch(SQLException ex){
                    
                }
            }
        }
        
        return status;
    }
}
