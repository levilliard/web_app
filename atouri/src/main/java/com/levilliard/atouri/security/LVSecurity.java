/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.levilliard.atouri.security;

/**
 *
 * @author levilliard
 */

import java.security.MessageDigest;
import javax.xml.bind.DatatypeConverter;

public class LVSecurity{
    public static String getHash(byte[] input, String algorithm){
        String hashValue = "";
        
        try{
            MessageDigest msg = MessageDigest.getInstance(algorithm);
            msg.update(input);
            byte[] digestByte = msg.digest();
            
            hashValue = DatatypeConverter.printHexBinary(digestByte).toLowerCase();
        }catch(Exception ex){
            
        }
        
        return hashValue;
    }
    
    public static boolean loginPassword(String password, String test){
        return password.equalsIgnoreCase(test);
    }
}
