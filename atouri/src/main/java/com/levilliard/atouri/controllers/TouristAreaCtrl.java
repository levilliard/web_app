       

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.levilliard.atouri.controllers;

/**
 *
 * @author levilliard
 */

import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.DELETE;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Consumes;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.PathParam;
import java.util.List;
import com.google.gson.Gson;
import com.levilliard.atouri.dao.TouristAreaDAO;
import com.levilliard.atouri.beans.TouristArea;


@Path("/tourist")
public class TouristAreaCtrl {
    
    private TouristAreaDAO taDAO;

    public TouristAreaCtrl(){
        taDAO = new TouristAreaDAO();
    }

    @GET
    @Path("/all/{cat}")
    @Produces(value = MediaType.APPLICATION_JSON)
    public Response doGetAll(@PathParam("cat") String cat) {
        List<TouristArea> m_list = taDAO.findByCriteria(cat);
        System.out.println("List Length: " + m_list.size());
        if(m_list != null && ! m_list.isEmpty()){
            Gson g = new Gson();
            return Response.status(Response.Status.CREATED).entity(g.toJson(m_list)).build();
        }else{
            return Response.serverError().entity("Not Found ").build();
        }
    }

    @DELETE
    @Path("/{id}")
    @Produces(MediaType.TEXT_PLAIN)
    public String delete(@PathParam("id") String id){
        boolean status = taDAO.delete(id);
        
        if(status){
            return "YES";
        }else{
            return "NO";
        }
    }
    
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public String create(TouristArea obj){
        boolean result = taDAO.create(obj);
        
        return result ? "Yes" : "No";
    }
    
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    public String update(TouristArea obj) {  
        boolean result = taDAO.update(obj);
        
        return result ? "Yes" : "No";
    }

}
