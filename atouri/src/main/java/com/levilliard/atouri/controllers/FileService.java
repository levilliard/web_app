package com.levilliard.atouri.controllers;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.POST;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Context;
import javax.servlet.ServletContext;

import com.sun.jersey.core.header.FormDataContentDisposition;
import com.sun.jersey.multipart.FormDataParam;

@Path("/file")
public class FileService {

    @Context
    private ServletContext context;

	@POST
	@Path(value = "upload/{directory}")
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	public Response uploadFile(
			@FormDataParam("file") InputStream uploadedInputStream,
			@FormDataParam("file") FormDataContentDisposition fileDetail, 
			@PathParam("directory") String directory) {

    	String path = context.getRealPath("/WEB-INF/assets/img/" + directory + "/") + "/" + fileDetail.getFileName();         

		//
		writeToFile(uploadedInputStream, path);

		String output = "File uploaded to : " + path;

		return Response.status(200).entity(output).build();

	}

	// save uploaded file to new location
	private void writeToFile(InputStream uploadedInputStream,
			String uploadedFileLocation) {

		try {
			OutputStream out = new FileOutputStream(new File(uploadedFileLocation));
			int read = 0;
			byte[] bytes = new byte[1024];

			out = new FileOutputStream(new File(uploadedFileLocation));
			while ((read = uploadedInputStream.read(bytes)) != -1) {
				out.write(bytes, 0, read);
			}
			out.flush();
			out.close();
		} catch (IOException e) {

			e.printStackTrace();
		}
	}

	@GET
    @Path(value = "download/{directory}/{name}")
    @Produces(MediaType.TEXT_PLAIN)
    public Response doUploadImg(@PathParam("directory") String directory, @PathParam("name") String name) throws IOException {
    	
    	String path = context.getRealPath("/WEB-INF/assets/img/" + directory) + "/" + name;         
        File file;
        file = new File(path);

        System.out.println("Path = " + path);

        if(!file.exists()){
        	file = new File(context.getRealPath("/WEB-INF/assets/img/" + directory) + "/" + "default.jpg");
        }

        if(file.exists()){
            Response.ResponseBuilder rpb = Response.ok((Object)file);
            return rpb.build();
        }else{
            return Response.serverError().entity("No Found->" + context.getRealPath("/WEB-INF/assets/img/" + directory)).build();
        }
    }
}