/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.levilliard.atouri.controllers;


import com.google.gson.Gson;
import javax.ws.rs.Consumes;
import javax.ws.rs.PUT;
import javax.ws.rs.POST;
import javax.ws.rs.DELETE;
import javax.ws.rs.PathParam;

import javax.ws.rs.core.Context;

import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.levilliard.atouri.beans.User;
import com.levilliard.atouri.dao.UserDAO;
import java.util.List;
import javax.servlet.ServletContext;

/**
 * REST Web Service
 *
 * @author levilliard
 */

@Path("user")
public class UserCtrl {
    @Context
    ServletContext context;
    
    private final UserDAO p_dao = new UserDAO();
    /**
     * Creates a new instance of GenericResource
     */
    public UserCtrl() {
        //System.out.println("The context: " + context);
    }

    @DELETE
    @Path("/{id}")
    @Produces(MediaType.TEXT_PLAIN)
    public String delete(@PathParam("id") String id){
        boolean status = p_dao.delete(id);
        
        if(status){
            return "YES";
        }else{
            return "NO";
        }
    }
    
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public String create(User obj){
        boolean result = p_dao.create(obj);
        
        return result ? "Yes" : "No";
    }
    
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String update(User obj) {  
        System.out.println(obj);
        boolean result = p_dao.update(obj);
        
        return result ? "Yes" : "No";
    }

    @GET
    @Path("/all")
    @Produces(value = MediaType.APPLICATION_JSON)
    public Response doGetAll() {
        List<User> m_list = p_dao.findAll();
        System.out.println("List Length: " + m_list.size());
        if(m_list != null && ! m_list.isEmpty()){
            Gson g = new Gson();
            return Response.status(Response.Status.CREATED).entity(g.toJson(m_list)).build();
        }else{
            return Response.serverError().entity("Not Found ").build();
        }
    }

    @POST
    @Path("/login")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)    
    public User doLogin(User obj) {
        return p_dao.userLogin(obj);
    }
}