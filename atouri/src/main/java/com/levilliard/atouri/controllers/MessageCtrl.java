/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.levilliard.atouri.controllers;


import com.google.gson.Gson;
import javax.ws.rs.Consumes;
import javax.ws.rs.PUT;
import javax.ws.rs.POST;
import javax.ws.rs.DELETE;
import javax.ws.rs.PathParam;

import javax.ws.rs.core.Context;

import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.levilliard.atouri.beans.Message;
import com.levilliard.atouri.dao.MessageDAO;
import java.util.List;
import javax.servlet.ServletContext;

/**
 * REST Web Service
 *
 * @author levilliard
 */

@Path("message")
public class MessageCtrl {
    @Context
    ServletContext context;
    
    private final MessageDAO p_dao = new MessageDAO();
    /**
     * Creates a new instance of GenericResource
     */
    public MessageCtrl() {
        //System.out.println("The context: " + context);
    }

    @DELETE
    @Path("/{id}")
    @Produces(MediaType.TEXT_PLAIN)
    public String delete(@PathParam("id") String id){
        boolean status = p_dao.delete(id);
        
        if(status){
            return "YES";
        }else{
            return "NO";
        }
    }
    
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public String create(Message obj){
        boolean result = p_dao.create(obj);
        
        return result ? "Yes" : "No";
    }
    
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String update(Message obj) {  
        System.out.println(obj);
        boolean result = p_dao.update(obj);
        
        return result ? "Yes" : "No";
    }


    @GET
    @Path("/user/{user_id}")
    @Produces(value = MediaType.APPLICATION_JSON)
    public Response doGetSomeData(@PathParam("user_id") String user_id) {
        List<Message> m_list = p_dao.findSomeData(user_id);
        System.out.println("List Length: " + m_list.size());
        if(m_list != null && ! m_list.isEmpty()){
            Gson g = new Gson();
            return Response.status(Response.Status.CREATED).entity(g.toJson(m_list)).build();
        }else{
            return Response.serverError().entity("Not Found ").build();
        }
    }

    @GET
    @Path("/all")
    @Produces(value = MediaType.APPLICATION_JSON)
    public Response doGetAll() {
        List<Message> m_list = p_dao.findAll();
        System.out.println("List Length: " + m_list.size());
        if(m_list != null && ! m_list.isEmpty()){
            Gson g = new Gson();
            return Response.status(Response.Status.CREATED).entity(g.toJson(m_list)).build();
        }else{
            return Response.serverError().entity("Not Found ").build();
        }
    }
}