package com.levilliard.atouri.util;

//import java.io.IOException;
//import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
//import java.util.Properties;

/*
 * The class above is in charge of connecting and disconnecting to the database
 */

public class DBConnection
{
	private static Connection mConnection;
	
	public static Connection getConnection()
	{
		if(mConnection != null)
		{
			return mConnection;
		}
		/*
		InputStream inputStream = DBConnection.class.getClassLoader().getResourceAsStream("config/db.properties");
		Properties mProperties = new Properties();
		*/
		try
		{
			//mProperties.load(inputStream);
			/*
			String driver = "org.postgresql.Driver"; //mProperties.getProperty("driver");
			String url = "jdbc:postgresql://localhost:5432/ATOURI";//mProperties.getProperty("url");
			String user = "postgres"; //mProperties.getProperty("user");
			String password = "supernova"; //mProperties.getProperty("password");
			
			Class.forName(driver);
			
			mConnection = DriverManager.getConnection(url, user, password);
			*/
			String username = "pouxfqgbidnjba";
            String password = "c380dcfa1f334c1fdc7f74157a18ec58267b82103752621d3e6294d5364f31a0";
            String dbUrl = "jdbc:postgresql://ec2-50-17-217-166.compute-1.amazonaws.com:5432/d640ki9oqko6ht?user=pouxfqgbidnjba&password=c380dcfa1f334c1fdc7f74157a18ec58267b82103752621d3e6294d5364f31a0&ssl=true&sslfactory=org.postgresql.ssl.NonValidatingFactory";
            mConnection = DriverManager.getConnection(dbUrl, username, password);

		}catch(SQLException e){
                    //e.printStackTrace();
        }
		return mConnection;
	}
	
	public static void closeConnection(Connection toBeClosed)
	{
		if(toBeClosed == null)
			return ;
		
		try
		{
			toBeClosed.close();
		}catch(SQLException e){
			e.printStackTrace();
		}
	}
}
