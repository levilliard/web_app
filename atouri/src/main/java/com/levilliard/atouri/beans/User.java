
package com.levilliard.atouri.beans;

public class User {
    
    private String hUserId;
    private String hUserFirstname;
    private String hUserLastname;
    private String hUserTitle;
    private String hUserSex;
    private String hUserPhone;
    private String hUserEmail;
    private String hUserUsername;
    private String hUserPassword;
    private String hUserCode;
    private String hUserImg;
    private String hUserDetails;
    private String hUserDate;

    public String gethUserId() {
        return hUserId;
    }

    public void sethUserId(String hUserId) {
        this.hUserId = hUserId;
    }

    public String gethUserFirstname() {
        return hUserFirstname;
    }

    public void sethUserFirstname(String hUserFirstname) {
        this.hUserFirstname = hUserFirstname;
    }

    public String gethUserLastname() {
        return hUserLastname;
    }

    public String gethUserTitle() {
        return hUserTitle;
    }

    public void sethUserTitle(String hUserTitle) {
        this.hUserTitle = hUserTitle;
    }

    public String gethUserSex() {
        return hUserSex;
    }

    public void sethUserSex(String hUserSex) {
        this.hUserSex = hUserSex;
    }

    public void sethUserLastname(String hUserLastname) {
        this.hUserLastname = hUserLastname;
    }

    public String gethUserPhone() {
        return hUserPhone;
    }

    public void sethUserPhone(String hUserPhone) {
        this.hUserPhone = hUserPhone;
    }

    public String gethUserEmail() {
        return hUserEmail;
    }

    public void sethUserEmail(String hUserEmail) {
        this.hUserEmail = hUserEmail;
    }

    public String gethUserUsername() {
        return hUserUsername;
    }

    public void sethUserUsername(String hUserUsername) {
        this.hUserUsername = hUserUsername;
    }

    public String gethUserPassword() {
        return hUserPassword;
    }

    public void sethUserPassword(String hUserPassword) {
        this.hUserPassword = hUserPassword;
    }

    public String gethUserCode() {
        return hUserCode;
    }

    public void sethUserCode(String hUserCode) {
        this.hUserCode = hUserCode;
    }

    public String gethUserImg() {
        return hUserImg;
    }

    public void sethUserImg(String hUserImg) {
        this.hUserImg = hUserImg;
    }

    public String gethUserDetails() {
        return hUserDetails;
    }

    public void sethUserDetails(String hUserDetails) {
        this.hUserDetails = hUserDetails;
    }

    public String gethUserDate() {
        return hUserDate;
    }

    public void sethUserDate(String hUserDate) {
        this.hUserDate = hUserDate;
    }

    @Override
    public String toString() {
        return "UserDAO{" + "hUserId=" + hUserId + ", hUserFirstname=" + hUserFirstname + ", hUserLastname=" + hUserLastname + ", hUserPhone=" + hUserPhone + ", hUserEmail=" + hUserEmail + ", hUserUsername=" + hUserUsername + '}';
    }
    
    
}
