/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.levilliard.atouri.beans;


public class Message {
    private int messageId;
    private int hUserId;
    private int touristAreaId;
    private String messageContent;
    private String messageDate;

    public int gethUserId() {
        return hUserId;
    }

    public void sethUserId(int hUserId) {
        this.hUserId = hUserId;
    }

    public int getTouristAreaId() {
        return touristAreaId;
    }

    public void setTouristAreaId(int touristAreaId) {
        this.touristAreaId = touristAreaId;
    }
    
    
    public int getMessageId() {
        return messageId;
    }

    public void setMessageId(int messageId) {
        this.messageId = messageId;
    }

    public String getMessageContent() {
        return messageContent;
    }

    public void setMessageContent(String messageContent) {
        this.messageContent = messageContent;
    }

    public String getMessageDate() {
        return messageDate;
    }

    public void setMessageDate(String messageDate) {
        this.messageDate = messageDate;
    }

    @Override
    public String toString() {
        return "Message{" + "messageId=" + messageId + ", messageIdUserSender=" + hUserId + ", messageContent=" + messageContent + ", messageDate=" + messageDate + '}';
    }
    
}
