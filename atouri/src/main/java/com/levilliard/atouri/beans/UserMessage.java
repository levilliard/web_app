/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.levilliard.atouri.beans;


public class UserMessage {
    private int userMessageId;
    private int hUserId;
    private String hUserUsername;
    private String userMessageContent;
    private String userMessageDate;

    public int gethUserId() {
        return hUserId;
    }

    public void sethUserId(int hUserId) {
        this.hUserId = hUserId;
    }

    public int getUserMessageId() {
        return userMessageId;
    }

    public void setUserMessageId(int userMessageId) {
        this.userMessageId = userMessageId;
    }

    public String gethUserUsername(){
    	return this.hUserUsername;
    }

    public void sethUserUsername(String hUserUsername){
    	this.hUserUsername = hUserUsername;
    }
    
    public String getUserMessageContent() {
        return userMessageContent;
    }

    public void setUserMessageContent(String userMessageContent) {
        this.userMessageContent = userMessageContent;
    }

    public String getUserMessageDate() {
        return userMessageDate;
    }

    public void setUserMessageDate(String userMessageDate) {
        this.userMessageDate = userMessageDate;
    }

    @Override
    public String toString() {
        return "UserMessage{" + "userMessageId=" + userMessageId + ", userMessageContent=" + userMessageContent + ", userMessageDate=" + userMessageDate + '}';
    }
    
}
