/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.levilliard.atouri.beans;

/**
 *
 * @author levilliard
 */
public class TouristArea {
    private String touristAreaId;   
    private String communeId;
    private String sectionCommId;
    private String touristAreaName; 
    private String touristAreaDesc; 
    private String touristAreaImg;  
    private String touristAreaDetails;  
    private String touristAreaLoc;  
    private String touristAreaLat;  
    private String touristAreaLong;  
    private String touristAreaCreatedBy;    
    private String touristAreaDateCreated;  
    private String touristAreaModifyBy;
    private String touristAreaDateModify;
    private String touristAreaType;
    private String touristAreaCat;
    private int touristAreaLike;
    private int touristAreaUnlike;

    public String getTouristAreaType() {
        return touristAreaType;
    }

    public void setTouristAreaType(String touristAreaType) {
        this.touristAreaType = touristAreaType;
    }

    public String getTouristAreaCat() {
        return touristAreaCat;
    }

    public void setTouristAreaCat(String touristAreaCat) {
        this.touristAreaCat = touristAreaCat;
    }
    
    public String getTouristAreaId() {
        return touristAreaId;
    }

    public void setTouristAreaId(String touristAreaId) {
        this.touristAreaId = touristAreaId;
    }

    public String getCommuneId() {
        return communeId;
    }

    public void setCommuneId(String communeId) {
        this.communeId = communeId;
    }

    public String getSectionCommId() {
        return sectionCommId;
    }

    public void setSectionCommId(String sectionCommId) {
        this.sectionCommId = sectionCommId;
    }

    public String getTouristAreaName() {
        return touristAreaName;
    }

    public void setTouristAreaName(String touristAreaName) {
        this.touristAreaName = touristAreaName;
    }

    public String getTouristAreaDesc() {
        return touristAreaDesc;
    }

    public void setTouristAreaDesc(String touristAreaDesc) {
        this.touristAreaDesc = touristAreaDesc;
    }

    public String getTouristAreaImg() {
        return touristAreaImg;
    }

    public void setTouristAreaImg(String touristAreaImg) {
        this.touristAreaImg = touristAreaImg;
    }

    public String getTouristAreaDetails() {
        return touristAreaDetails;
    }

    public void setTouristAreaDetails(String touristAreaDetails) {
        this.touristAreaDetails = touristAreaDetails;
    }

    public String getTouristAreaLoc() {
        return touristAreaLoc;
    }

    public void setTouristAreaLoc(String touristAreaLoc) {
        this.touristAreaLoc = touristAreaLoc;
    }

    public String getTouristAreaLat() {
        return touristAreaLat;
    }

    public void setTouristAreaLat(String touristAreaLat) {
        this.touristAreaLat = touristAreaLat;
    }

    public String getTouristAreaLong() {
        return touristAreaLong;
    }

    public void setTouristAreaLong(String touristAreaLong) {
        this.touristAreaLong = touristAreaLong;
    }

    public String getTouristAreaCreatedBy() {
        return touristAreaCreatedBy;
    }

    public void setTouristAreaCreatedBy(String touristAreaCreatedBy) {
        this.touristAreaCreatedBy = touristAreaCreatedBy;
    }

    public String getTouristAreaDateCreated() {
        return touristAreaDateCreated;
    }

    public void setTouristAreaDateCreated(String touristAreaDateCreated) {
        this.touristAreaDateCreated = touristAreaDateCreated;
    }

    public String getTouristAreaModifyBy() {
        return touristAreaModifyBy;
    }

    public void setTouristAreaModifyBy(String touristAreaModifyBy) {
        this.touristAreaModifyBy = touristAreaModifyBy;
    }

    public String getTouristAreaDateModify() {
        return touristAreaDateModify;
    }

    public void setTouristAreaDateModify(String touristAreaDateModify) {
        this.touristAreaDateModify = touristAreaDateModify;
    }

    public int getTouristAreaLike() {
        return touristAreaLike;
    }

    public void setTouristAreaLike(int touristAreaLike) {
        this.touristAreaLike = touristAreaLike;
    }

    public int getTouristAreaUnlike() {
        return touristAreaUnlike;
    }

    public void setTouristAreaUnlike(int touristAreaUnlike) {
        this.touristAreaUnlike = touristAreaUnlike;
    }

    @Override
    public String toString() {
        return "TouristArea{" + "touristAreaId=" + touristAreaId + ", touristAreaName=" + touristAreaName + ", touristAreaDesc=" + touristAreaDesc + ", touristAreaDetails=" + touristAreaDetails + ", touristAreaLoc=" + touristAreaLoc + ", touristAreaLat=" + touristAreaLat + ", touristAreaLong=" + touristAreaLong + '}';
    }
    
    
}
