/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.levilliard.atouri.beans;

/**
 *
 * @author levilliard
 */
public class UserLike {
    
    private int userLikeId;
    private int  hUserId;
    private int touristAreaId;
    private int userLikeValue;
    private int userUnlike;
    private String userLikeDateCreated;

    public String getUserLikeDateCreated() {
        return userLikeDateCreated;
    }

    public void setUserLikeDateCreated(String userLikeDateCreated) {
        this.userLikeDateCreated = userLikeDateCreated;
    }

    public int getUserLikeId() {
        return userLikeId;
    }

    public void setUserLikeId(int userLikeId) {
        this.userLikeId = userLikeId;
    }

    public int gethUserId() {
        return hUserId;
    }

    public void sethUserId(int hUserId) {
        this.hUserId = hUserId;
    }

    public int getTouristAreaId() {
        return touristAreaId;
    }

    public void setTouristAreaId(int touristAreaId) {
        this.touristAreaId = touristAreaId;
    }

    public int getUserLikeValue() {
        return userLikeValue;
    }

    public void setUserLikeValue(int userLikeValue) {
        this.userLikeValue = userLikeValue;
    }

    public int getUserUnlike() {
        return userUnlike;
    }

    public void setUserUnlike(int userUnlike) {
        this.userUnlike = userUnlike;
    }
        
}
